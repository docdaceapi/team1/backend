import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Member } from 'src/members/entities/member.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  qty: number;

  @Column()
  memberDiscount: number;

  @Column()
  totalBefore: number;

  @Column({ nullable: true })
  receivedAmount?: number;

  @Column()
  change: number;

  @Column()
  total: number;

  @Column()
  paymentType: string;

  @Column({ nullable: true }) // Set nullable to true
  memberId: number;

  @Column()
  userId: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  member?: Member | null;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];

  @ManyToOne(() => User, (user) => user.orders)
  user: User;
}
