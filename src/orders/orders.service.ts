import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { OrderItem } from './entities/orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';
import { Member } from 'src/members/entities/member.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    @InjectRepository(Member) private membersRepository: Repository<Member>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const order = new Order();
    const user = await this.usersRepository.findOneBy({
      id: createOrderDto.userId,
    });
    let member = null;
    if (createOrderDto.memberId === 0) {
      member = null;
    } else {
      member = await this.membersRepository.findOneBy({
        id: createOrderDto.memberId,
      });
    }
    order.member = member;
    order.user = user;
    order.userId = createOrderDto.userId;
    order.memberId = createOrderDto.memberId;
    order.memberDiscount = createOrderDto.memberDiscount;
    order.paymentType = createOrderDto.paymentType;
    order.receivedAmount = createOrderDto.receivedAmount;
    order.totalBefore = createOrderDto.totalBefore;
    order.total = 0;
    order.change = createOrderDto.change;
    order.qty = 0;
    order.orderItems = [];
    for (const oi of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.product = await this.productsRepository.findOneBy({
        id: oi.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.sweet = oi.sweet;
      orderItem.qty = oi.qty;
      orderItem.total = orderItem.price * orderItem.qty;
      await this.orderItemsRepository.save(orderItem);
      order.orderItems.push(orderItem);
      order.total += orderItem.total;
      order.qty += orderItem.qty;
    }
    return this.ordersRepository.save(order);
  }

  findAll() {
    return this.ordersRepository.find({
      relations: {
        orderItems: true,
      },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOneOrFail({
      where: { id },
      relations: { orderItems: true },
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrder = await this.ordersRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
