export class CreateOrderDto {
  orderItems: {
    productId: number;
    qty: number;
    sweet: string;
  }[];
  memberId: number;
  userId: number;
  memberDiscount: number;
  change: number;
  paymentType: string;
  receivedAmount: number;
  totalBefore: number;
  total: number;
}
