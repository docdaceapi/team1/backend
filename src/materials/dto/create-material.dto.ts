export class CreateMaterialDto {
  name: string;

  quantity: number;

  qMin: number;

  qMax: number;
}
