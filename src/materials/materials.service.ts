import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Material } from './entities/materials.entity';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}

  create(createMaterialDto: CreateMaterialDto) {
    const material = new Material();
    material.name = createMaterialDto.name;
    material.quantity = createMaterialDto.quantity;
    material.qMin = createMaterialDto.qMin;
    material.qMax = createMaterialDto.qMax;

    return this.materialsRepository.save(material);
  }

  findAll() {
    return this.materialsRepository.find();
  }

  findOne(id: number) {
    return this.materialsRepository.findOneByOrFail({
      id,
    });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.materialsRepository.findOneOrFail({
      where: { id },
    });
    material.name = updateMaterialDto.name;
    material.quantity = updateMaterialDto.quantity;
    material.qMax = updateMaterialDto.qMax;
    material.qMin = updateMaterialDto.qMin;
    await this.materialsRepository.save(material);
    const result = await this.materialsRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.materialsRepository.findOneOrFail({
      where: { id },
    });
    await this.materialsRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
