import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Member } from './entities/member.entity';
import { Repository } from 'typeorm';
import { UpdateMemberDto } from './dto/update-member.dto';

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(Member)
    private membersRepository: Repository<Member>,
  ) {}

  create(createMemberDto: CreateMemberDto) {
    const member = new Member();
    member.name = createMemberDto.name;
    member.tel = createMemberDto.tel;

    return this.membersRepository.save(member);
  }

  findAll() {
    return this.membersRepository.find();
  }

  findOne(id: number) {
    return this.membersRepository.findOneByOrFail({
      id,
    });
  }

  async update(id: number, UpdateMemberDto: UpdateMemberDto) {
    const member = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    member.name = UpdateMemberDto.name;
    member.tel = UpdateMemberDto.tel;
    await this.membersRepository.save(member);
    const result = await this.membersRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteMembers = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    await this.membersRepository.remove(deleteMembers);
    return deleteMembers;
  }
}
