import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Patch,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { MembersService } from './members.service';
import { UpdateMemberDto } from './dto/update-member.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('members')
export class MembersController {
  constructor(private readonly membersService: MembersService) {}

  // Create
  @Post()
  create(@Body() createMembersDto: CreateMemberDto) {
    return this.membersService.create(createMembersDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.membersService.findAll();
  }

  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.membersService.findOne(+id);
  }

  // Partial Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateMemberDto) {
    return this.membersService.update(+id, updateUserDto);
  }

  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.membersService.remove(+id);
  }
}
